# Lambda-C Decay notebooks
HPSS project

## Interactive notebooks
The lecture course offer many exercises where the visial presentation of the problem
helps to get deeper undersanding. We support the lecture manuscript with
the set of interactive notebooks. The equations and plots in the notebooks go along
with the corresponding chapters in the lecture notes.
### Jupyter-notebook with julia kernel in browser
  * Go to the `juliabox` webpage [Juliabox](https://juliabox.com/),
  * Sign in using your `Google`/`GitHub`/`LinkedIn` account,
  * Check `Git`-tab on the top of the page,
  * Clone git repository from the `GitLab` by inserting the [link](https://gitlab.cern.ch/mimikhas/LambdaCdecay.git)
```bash
https://gitlab.cern.ch/mimikhas/LambdaCdecay.git
```
    to the `Git Clone URL` field and pressing plus sign
    and clicking to the plus button.
    When the operation is complete you can `Close` the window.
  * If the previous step went correctly you will see the folder `￼LambdaCdecay`
    in the browser. Enter this folder to access the notebooks.
  * Click the required exercise file. When the notebook is opened,
    the kernel will start automatically.
  * Minimal set of short cuts:
    * `Enter` to edit cell content (edit mode), 'Esc' to get to command mode.
    * `Shift-Enter` to evaluate a cell.
    * `b` to create a new cell below, 'a' to create a new cell above.
    * `m` in the command mode will change cell type to Markdown,
      `y` will change the cell type to Run cell.
    * `c` to copy the current cell, \ovalbox{v} to insert the copied cell below.

The examples are written using `julia` language.
 * Excellent [tutorial video](https://www.youtube.com/watch?v=puMIBYthsjQ) about `julia` language by David Sanders,
 * [More instructions](http://jupyter-notebook.readthedocs.io/en/latest/notebook.html) for `jupyter notebook`.
